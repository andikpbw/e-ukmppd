//
//  ContentView.swift
//  E-UKMPPD
//
//  Created by MacOS on 03/04/20.
//  Copyright © 2020 MacOS. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World1!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
